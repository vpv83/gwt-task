package cleancodematters.datagriddemo.server.domain;

/**
 * Base persisted entity class.
 *
 * @author mgushinets
 * @since  15/05/2014
 */
public class PersistedEntity {

    protected Long version;

    protected Long id;

    /**
     * Get current version of persisted entity.
     *
     * @return persisted entity version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Set current version for persisted entity.
     *
     * @param version new version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * Get id of persisted entity.
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set id for persisted entity.
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
}
