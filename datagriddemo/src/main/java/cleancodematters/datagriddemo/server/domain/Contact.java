package cleancodematters.datagriddemo.server.domain;

import java.util.LinkedList;
import java.util.List;

/**
 * Contact entity class.
 *
 * @author mgushinets
 * @since  15/05/2014
 */
public class Contact extends PersistedEntity {

    /**
     * contact first name
     */
    private String firstName;

    /**
     * contact last name
     */
    private String lastName;

    public Contact() {
    }

    /**
     * Get contact first name.
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set contact first name.
     *
     * @param firstName first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get contact last name.
     *
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set contact last name.
     *
     * @param lastName last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
