package cleancodematters.datagriddemo.server.domain;

import cleancodematters.datagriddemo.shared.ColumnName;

/**
 * Page request implementation.
 *
 * @author mgushinets
 * @since 15/05/2014
 */
public class Page {

    private int offset;

    private int limit;

    private boolean asc;

    private ColumnName sort;

    /**
     * Get offset for data to be returned.
     *
     * @return data offset
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Set offset for data to be returned.
     *
     * @param offset data offset
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * Get limit of data to be returned.
     *
     * @return limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Set limit of data to be returned.
     *
     * @param limit limit
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * Get the sorting order (asc or desc).
     * In case of asc method return <code>true</code>, otherwise <code>false</code> is returned.
     *
     * @return sort order
     */
    public boolean isAsc() {
        return asc;
    }

    /**
     * Set the sorting order.
     *
     * @param asc sort order
     */
    public void setAsc(boolean asc) {
        this.asc = asc;
    }

    /**
     * Get sorting column.
     *
     * @return sorting column
     */
    public ColumnName getSort() {
        return sort;
    }


    /**
     * Set sorting column.
     *
     * @param sort sorting column
     */
    public void setSort(ColumnName sort) {
        this.sort = sort;
    }
}
