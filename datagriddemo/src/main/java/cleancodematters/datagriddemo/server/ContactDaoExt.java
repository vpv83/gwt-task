package cleancodematters.datagriddemo.server;

import cleancodematters.datagriddemo.server.domain.Contact;
import cleancodematters.datagriddemo.server.domain.Page;

import java.util.List;

/**
 * @author mgushitets
 * @since 15/05/2014
 */
public interface ContactDaoExt {

    List<Contact> findAll(Page page);

    Contact findOne(Long id);
}
