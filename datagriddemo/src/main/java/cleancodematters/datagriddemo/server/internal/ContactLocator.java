package cleancodematters.datagriddemo.server.internal;

import cleancodematters.datagriddemo.server.ContactDao;
import cleancodematters.datagriddemo.server.ContactDaoImpl;
import cleancodematters.datagriddemo.server.domain.Contact;

import com.google.web.bindery.requestfactory.shared.Locator;
import org.apache.commons.lang3.NotImplementedException;


public class ContactLocator extends Locator<Contact, Long> {

    // TODO : spring JPA injection
    ContactDao contactDao = new ContactDaoImpl();

    @Override
    public Contact create(Class<? extends Contact> clazz) {
        return new Contact();
    }

    @Override
    public Contact find(Class<? extends Contact> clazz, Long id) {
        return contactDao.findOne(id);
    }

    @Override
    public Class<Contact> getDomainType() {
        return Contact.class;
    }

    @Override
    public Long getId(Contact domainObject) {
        return domainObject.getId();
    }

    @Override
    public Class<Long> getIdType() {
        return Long.class;
    }

    @Override
    public Object getVersion(Contact contact) {
        return contact.getVersion();
    }
}
