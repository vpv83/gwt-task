package cleancodematters.datagriddemo.server.internal;


import cleancodematters.datagriddemo.server.ContactDao;
import cleancodematters.datagriddemo.server.ContactDaoImpl;
import com.google.web.bindery.requestfactory.server.RequestFactoryServlet;
import com.google.web.bindery.requestfactory.shared.ServiceLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public class DaoLocator implements ServiceLocator {

    private ContactDao contactDao = new ContactDaoImpl();

    @Override
    public Object getInstance(Class<?> clazz) {
        return contactDao;
    }

}
