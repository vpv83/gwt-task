package cleancodematters.datagriddemo.server;

import cleancodematters.datagriddemo.server.domain.Contact;
import cleancodematters.datagriddemo.server.domain.Page;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mgushinets
 * @since 05/15/2014
 */
public class ContactDaoImpl implements ContactDao {

    private List<Contact> contacts;

    public ContactDaoImpl() {
        generate();
    }

    @Override
    public List<Contact> findAll(Page page) {

        int startIndex = page.getOffset() > contacts.size() - 1 ? contacts.size() - 1 : page.getOffset();
        int endIndex = page.getOffset() + page.getLimit() > contacts.size() - 1 ? contacts.size() - 1 :
                page.getOffset() + page.getLimit();

        System.out.println("find all method : start " + startIndex + " endIndex = " + endIndex);

        return contacts.subList(startIndex, endIndex);
    }

    public Contact findOne(final Long id) {
        return Iterators.find(contacts.iterator(), new Predicate<Contact>() {
            @Override
            public boolean apply(Contact o) {
                return o.getId().equals(id);
            }
        });
    }


    private void generate() {
        if (contacts == null) {
            contacts = new ArrayList<Contact>();

            for (long i = 0; i < 1000; i++) {
                Contact c = new Contact();
                c.setId(i);
                c.setVersion(1L);
                c.setFirstName("firstname " + i);
                c.setLastName("lastname" + i);
                contacts.add(c);
            }
        }
    }

}
