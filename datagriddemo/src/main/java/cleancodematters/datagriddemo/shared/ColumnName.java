package cleancodematters.datagriddemo.shared;

/**
 * Set of possible table columns for sorting.
 *
 */
public enum ColumnName {

    FIRST_NAME,

    LAST_NAME;
}
