package cleancodematters.datagriddemo.client.proxy;

import cleancodematters.datagriddemo.server.domain.Page;
import cleancodematters.datagriddemo.shared.ColumnName;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;


@ProxyFor(Page.class)
public interface PageProxy extends ValueProxy {

    int getOffset();

    void setOffset(int offset);

    int getLimit();

    void setLimit(int limit);

    boolean isAsc();

    void setAsc(boolean asc);

    ColumnName getSort();

    void setSort(ColumnName sort);
}
