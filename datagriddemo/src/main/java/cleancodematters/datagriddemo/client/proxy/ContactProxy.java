package cleancodematters.datagriddemo.client.proxy;

import java.util.List;

import cleancodematters.datagriddemo.server.domain.Contact;
import cleancodematters.datagriddemo.server.internal.ContactLocator;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;


@ProxyFor(value = Contact.class, locator = ContactLocator.class)
public interface ContactProxy extends EntityProxy {

    Long getId();

    String getFirstName();

    void setFirstName(String name);

    String getLastName();

    void setLastName(String lastName);

}
