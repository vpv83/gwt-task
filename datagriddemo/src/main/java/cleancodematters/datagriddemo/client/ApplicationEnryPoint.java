package cleancodematters.datagriddemo.client;

import java.util.HashMap;
import java.util.Map;

import cleancodematters.datagriddemo.client.ui.data.ContactDataProvider;
import cleancodematters.datagriddemo.client.proxy.ContactProxy;
import cleancodematters.datagriddemo.client.ui.DynamicLoadingDataScrollHandler;
import cleancodematters.datagriddemo.client.ui.ScrollableDataGrid;
import cleancodematters.datagriddemo.shared.ColumnName;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.view.client.ProvidesKey;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ApplicationEnryPoint implements EntryPoint {

    private Map<Column<?, ?>, ColumnName> columnToSortKeyMapping = new HashMap<Column<?, ?>, ColumnName>();

    public void onModuleLoad() {

        RootPanel container = RootPanel.get("content");

        // create contact data provider
        final ContactDataProvider contactDataProvider = new ContactDataProvider(createRequestFactory());

        // create data grid component
        final DataGrid<ContactProxy> contactTable = createContactTable(contactDataProvider);

        container.add(contactTable);
    }

    private ScrollableDataGrid<ContactProxy> createContactTable(final ContactDataProvider contactDataProvider) {

        final ScrollableDataGrid<ContactProxy> contactTable = new ScrollableDataGrid<ContactProxy>(new ProvidesKey<ContactProxy>() {
            @Override
            public Object getKey(ContactProxy item) {
                return item == null ? null : item.getId();
            }
        });


        // enable dynamic downloading data by changing visible range
        final DynamicLoadingDataScrollHandler dynamicLoadingDataScrollHandler =
            new DynamicLoadingDataScrollHandler(contactTable.getScrollPanel(),
                new DynamicLoadingDataScrollHandler.IncreaseRange() {
                    @Override
                    public void increase() {
                        contactTable.setVisibleRange( 0 ,
                                contactTable.getVisibleRange().getLength() +
                                        DynamicLoadingDataScrollHandler.INCREMENT_SIZE);
                    }});
        contactTable.getScrollPanel().addScrollHandler(dynamicLoadingDataScrollHandler);

        contactTable.addColumnSortHandler(new ColumnSortEvent.Handler() {
            @Override
            public void onColumnSort(ColumnSortEvent event) {
                if( event.getColumnSortList().size() > 0 ) {

                    ColumnSortList.ColumnSortInfo columnSortInfo = event.getColumnSortList().get( 0 );
                    ColumnName sortKey = columnToSortKeyMapping.get( columnSortInfo.getColumn() );

                    contactDataProvider.changeSort(contactTable, sortKey, columnSortInfo.isAscending());
                    contactTable.setVisibleRange(0, ContactDataProvider.INITIAL_DATA_COUNT);
                    dynamicLoadingDataScrollHandler.reset();
                }

            }
        });

        contactTable.setPageSize(ContactDataProvider.INITIAL_DATA_COUNT);


        contactTable.setWidth("100%");
        contactTable.setHeight("400px");

        TextColumn<ContactProxy> idColumn = new TextColumn<ContactProxy>() {
            @Override
            public String getValue(ContactProxy object) {
                return String.valueOf(object.getId());
            }
        };
        idColumn.setSortable(false);

        TextColumn<ContactProxy> firstNameColumn = new TextColumn<ContactProxy>() {
            @Override
            public String getValue(ContactProxy object) {
                return object.getFirstName();
            }
        };
        firstNameColumn.setSortable(true);

        TextColumn<ContactProxy> lastNameColumn = new TextColumn<ContactProxy>() {
            @Override
            public String getValue(ContactProxy object) {
                return object.getLastName();
            }
        };
        lastNameColumn.setSortable(true);


        // TODO: add localization
        contactTable.addColumn(idColumn, "Id");
        contactTable.addColumn(firstNameColumn, "First Name");
        contactTable.addColumn(lastNameColumn , "Second Name");

        //TODO: need to change either with percentage or smth else
        contactTable.setColumnWidth(idColumn, "10%");
        contactTable.setColumnWidth(firstNameColumn, "45%");
        contactTable.setColumnWidth(lastNameColumn , "45%");

        columnToSortKeyMapping.put(idColumn, ColumnName.FIRST_NAME);
        columnToSortKeyMapping.put(lastNameColumn, ColumnName.LAST_NAME);

        contactTable.getColumnSortList().push(idColumn);

        contactDataProvider.addDataDisplay(contactTable);

        return contactTable;
    }

    private ContactRequestFactory createRequestFactory() {
        ContactRequestFactory result = GWT.create(ContactRequestFactory.class);
        result.initialize(new SimpleEventBus());
        return result;
    }
}
