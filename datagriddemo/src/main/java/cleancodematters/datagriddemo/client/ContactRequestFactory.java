package cleancodematters.datagriddemo.client;

import cleancodematters.datagriddemo.client.proxy.ContactProxy;
import cleancodematters.datagriddemo.client.proxy.PageProxy;
import cleancodematters.datagriddemo.server.ContactDao;
import cleancodematters.datagriddemo.server.internal.DaoLocator;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.RequestFactory;
import com.google.web.bindery.requestfactory.shared.Service;

import java.util.List;


public interface ContactRequestFactory extends RequestFactory {

    @Service(value = ContactDao.class, locator = DaoLocator.class)
    public interface ContactRequestContext extends RequestContext {

        Request<List<ContactProxy>> findAll(PageProxy page);
    }

    ContactRequestContext getContactRequestFactory();
}
