package cleancodematters.datagriddemo.client.ui;

import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.user.client.ui.HasVerticalScrolling;
import com.google.gwt.user.client.ui.ScrollPanel;

/**
 * @author mgushinets
 * @since 05/15/2014
 */
public class DynamicLoadingDataScrollHandler implements ScrollHandler {


    /** Increment data batch size */
    public static final int INCREMENT_SIZE = 100;

    /** maximum scrollable position */
    private int maxScrollablePosition = 0;

    /** encapsulation to increase data range */
    private IncreaseRange increaseRange;

    /** scrollable component */
    private HasVerticalScrolling scrollable;

    public DynamicLoadingDataScrollHandler(HasVerticalScrolling scrollable, IncreaseRange increaseRange) {
        this.scrollable = scrollable;
        this.increaseRange = increaseRange;
    }

    @Override
    public void onScroll(ScrollEvent event) {

        int currentScrollingPosition = scrollable.getVerticalScrollPosition();

        if (maxScrollablePosition < currentScrollingPosition) {

            maxScrollablePosition = currentScrollingPosition;

            double delta = scrollable.getMaximumVerticalScrollPosition()
                    - scrollable.getVerticalScrollPosition();

            if (delta == 0) {
                increaseRange.increase();
            }
        }
    }

    public void reset() {
        maxScrollablePosition = 0;
    }

    /**
     * interface to update data provider range
     */
    public interface IncreaseRange {
        // may be it need get current scrollable parameters
        // in out case it is not necessary
        void increase();
    }
}
