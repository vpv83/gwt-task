package cleancodematters.datagriddemo.client.ui;

import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.HeaderPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.view.client.ProvidesKey;

/**
 * @author mgushinets
 * @since 05/15/2014
 */
public class ScrollableDataGrid<T> extends DataGrid<T> {

    public ScrollableDataGrid(ProvidesKey<T> keyProvider) {
        super(keyProvider);
    }

    public ScrollPanel getScrollPanel() {
        HeaderPanel header = (HeaderPanel) getWidget();
        return (ScrollPanel) header.getContentWidget();
    }
}
