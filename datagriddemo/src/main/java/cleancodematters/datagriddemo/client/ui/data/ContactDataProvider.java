package cleancodematters.datagriddemo.client.ui.data;

import java.util.List;

import cleancodematters.datagriddemo.client.ContactRequestFactory;
import cleancodematters.datagriddemo.client.proxy.ContactProxy;
import cleancodematters.datagriddemo.client.proxy.PageProxy;
import cleancodematters.datagriddemo.shared.ColumnName;

import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;

/**
 * @author mgushinets
 * @since 15/05/2015
 */
public class ContactDataProvider extends AsyncDataProvider<ContactProxy> {

    private final ContactRequestFactory factory;

    private boolean sortAsc;

    private ColumnName sortKey;

    public static final int INITIAL_DATA_COUNT = 100;

    public ContactDataProvider(ContactRequestFactory factory) {
        this.factory = factory;
        this.sortAsc = true;
        this.sortKey = ColumnName.FIRST_NAME;
    }


    /**
     * update table data
     *
     * @param display
     */
    @Override
    protected void onRangeChanged(final HasData<ContactProxy> display) {

        // get new range
        final Range range = display.getVisibleRange();

        // create request context
        ContactRequestFactory.ContactRequestContext contactRequestContext = factory.getContactRequestFactory();

        // create page request for data to be returned
        PageProxy page = createPage(range, contactRequestContext);

        // get remote data using request factory
        Request<List<ContactProxy>> request = contactRequestContext.findAll(page);

        // update table data
        request.fire(new Receiver<List<ContactProxy>>() {

            @Override
            public void onSuccess(List<ContactProxy> response) {

                display.setRowCount(response.size(), false);

                display.setRowData(range.getStart(), response);
            }
        });
    }

    private PageProxy createPage(final Range range, RequestContext context) {

        PageProxy page = context.create(PageProxy.class);

        page.setOffset(range.getStart());
        page.setLimit(range.getLength());
        page.setSort(sortKey);
        page.setAsc(sortAsc);

        return page;
    }

    public void changeSort(HasData<ContactProxy> display, ColumnName sortKey, boolean ascending) {
        this.sortKey = sortKey;
        this.sortAsc = ascending;
    }

}
